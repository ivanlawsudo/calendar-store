import React, { useState, Fragment } from "react";
import Calendar from "react-calendar";
import item2 from "../images/item2.png";
import "../index.css";

const Item2 = () => {
  const [date, setDate] = useState(new Date());

  function onChange(date) {
    setDate(date);
  }

  console.log(date);

  return (
    <Fragment>
      <Fragment>
        <img src={item2} className="item-img" alt="item2" />
        <button
          type="button"
          className="btn btn-primary mx-auto mt-3"
          data-toggle="modal"
          data-target="#item1Modal"
        >
          Book Now
        </button>
      </Fragment>

      <div
        className="modal fade"
        id="item1Modal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="item1ModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <Calendar onChange={onChange} value={date} />
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Item2;
