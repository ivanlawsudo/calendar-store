/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect, Fragment } from "react";
import { DateRange } from "react-date-range";
import item1 from "../images/item1.png";
import item1_disabled from "../images/item1_disabled.png";
import "../index.css";

const Item1 = () => {
  const [date, setDate] = useState({});
  const [amt, setAmt] = useState(1);
  let [closeCounter, setCloseCounter] = useState(0);

  function dateChange(range) {
    setDate(range);
    setCloseCounter(closeCounter + 1);
  }

  useEffect(() => {
    if (closeCounter > 2) setCloseCounter(0);
  }, [closeCounter]);

  return (
    <Fragment>
      {amt <= 0 ? (
        <Fragment>
          <img src={item1_disabled} className="item-img" alt="item1" />
          <h6>Available: {amt}</h6>
        </Fragment>
      ) : (
        <Fragment>
          <img src={item1} className="item-img" alt="item1" />
          <h6>Available: {amt}</h6>
          <button
            type="button"
            className="btn btn-primary mx-auto mt-3"
            data-toggle="modal"
            data-target="#item1Modal"
          >
            Book Now
          </button>
        </Fragment>
      )}

      <div
        className="modal fade"
        id="item1Modal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="item1ModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header mx-auto">Select Date Range</div>
            <div className="modal-body">
              <DateRange onChange={dateChange} calendars="1" />
            </div>
            {closeCounter === 2 && (
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-primary mx-auto"
                  data-dismiss="modal"
                  onClick={() => setCloseCounter(closeCounter + 1)}
                >
                  Add to Cart
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Item1;
