import React from "react";
import Item1 from "./Item1";
import Item2 from "./Item2";

const Store = () => {
  return (
    <div className="container">
      <div className="row mx-auto">
        <div className="col-md-6 item">
          <Item1 />
        </div>
        <div className="col-md-6 item">
          <Item2 />
        </div>
      </div>
    </div>
  );
};

export default Store;
