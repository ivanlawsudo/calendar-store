import React from "react";
import { Store, ShoppingCart } from "./components";
// import logo from "./logo.svg";

function App() {
  return (
    <div className="App">
      {/* <img src={logo} className="App-logo" alt="logo" /> */}
      <h1 className="header mt-5">Store</h1>
      <Store />
      <h1 className="header mt-5">Shopping Cart</h1>
      <ShoppingCart />
    </div>
  );
}

export default App;
