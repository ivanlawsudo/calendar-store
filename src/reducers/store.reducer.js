import * as actions from "../actions";

const initValues = {
  item1: 0,
  item2: 0
};

export default (store = initValues, action) => {
  switch (action.type) {
    case actions.CLEAR_STORE:
      return {
        item1: 0,
        item2: 0
      };
    case actions.INCREMENT_ITEM_1:
      const item1Increment = initValues.item1 + 1;
      return {
        ...initValues,
        item1: item1Increment
      };
    case actions.DECREMENT_ITEM_1:
      const item1Decrement = initValues.item1 - 1;
      return {
        ...initValues,
        item1: item1Decrement
      };
    default:
      return store;
  }
};
